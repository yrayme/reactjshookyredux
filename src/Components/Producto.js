import React from 'react';
import { useHistory } from 'react-router-dom';

//Redux
import { useDispatch } from 'react-redux';
import { borrarProductoAction, editarProductoAction } from '../actions/act_productos';
import Swal from 'sweetalert2';

const Producto = ({ productos }) => {


    const { nombre, precio, id } = productos

    const dispatch = useDispatch();
    const history = useHistory();

    // Confirmar si desea eliminarlo
    const confirmarEliminarProducto = id => {

        //preguntar al usuario
        Swal.fire({
            title: '¿Estas Seguro?',
            text: "Un porducto que se elimina no se puede recuperar",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, eliminar!',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {

                //pasarlo al action
                dispatch(borrarProductoAction(id))              
            }
        })

    }

    //Función que redirige de forma programada
    const redireccionarEdición = producto => {
        dispatch(editarProductoAction(producto));
        history.push(`/productos/editar/${id}`)
    }

    return (
        <tr>
            <td>{nombre}</td>
            <td><span className='font-weight-bold'> $ {precio}</span></td>
            <td className='acciones'>
                <button type='button'
                onClick={() => redireccionarEdición(productos)}
                className='btn btn-primary mr-2'>
                    Editar
                </button>
                <button
                    type='button'
                    className='btn btn-danger'
                    onClick={() => confirmarEliminarProducto(id)}
                >
                    Eliminar
                </button>
            </td>
        </tr>
    );
}

export default Producto;