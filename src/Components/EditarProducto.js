import React, { useState } from 'react';
import { actualizarProductoAction } from '../actions/act_productos';

//Redux
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';


const EditarProducto = () => {

    const history = useHistory();
    const dispatch = useDispatch();
    // nuevo state de producto 
    
    const productoEditar = useSelector(state => state.productos.productoEditar);    

    // if (!productoEditar) return null;
    const [productos, guardarProducto] = useState({
        nombre: productoEditar.nombre,
        precio: productoEditar.precio,
        id: productoEditar.id
    });

    
    // const [nombre, guardarNombre] = useState(productoEditar.nombre);
    // const [precio, guardarPrecio] = useState(productoEditar.precio);


    // //llenar el state automaticamente
    // useEffect(() => {
    //     guardarProducto(productoEditar)
    // }, [productoEditar])


    //Leer Datos del formulario
    const onChangeFormulario = e => {
        // alert(JSON.stringify(productos))
        guardarProducto({
            ...productos,
            [e.target.name] : e.target.value
        })
    }

    // const {nombre, precio} = productoEditar

    //cuando el usuario haga submit
    const submitEditarProducto = e => {
        e.preventDefault();

        //validar formulario

        // if(nombre.trim() === '' || precio <= 0){
        //     return;
        // }

        //si no hay errores

        //actualizar producto

        dispatch(actualizarProductoAction(productos));

        // //redireccionar
        history.push('/');
    }

    return (
        <div className='row justify-content-center'>
            <div className='col-md-8'>
                <div className='card'>
                    <div className='card-body'>
                        <h2 className='text-center mb-4 font-weight-bold'>
                            Editar Producto
                        </h2>
                        <form
                            onSubmit={submitEditarProducto}
                        >
                            <div className='form-group'>
                                <label>Nombre Producto</label>
                                <input
                                    type='text'
                                    className='form-control'
                                    placeholder='Nombre Producto'
                                    name='nombre'
                                    value={productos.nombre}
                                    onChange={onChangeFormulario}
                                />
                            </div>
                            <div className='form-group'>
                                <label>Precio Producto</label>
                                <input
                                    type='number'
                                    className='form-control'
                                    placeholder='Precio Producto'
                                    name='precio'
                                    value={productos.precio}
                                    onChange={onChangeFormulario}
                                />
                            </div>
                            <button
                                type='submit'
                                className='btn btn-primary font-weight-bold text-uppercase d-block w-100'
                            >Guardar Cambios</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default EditarProducto;