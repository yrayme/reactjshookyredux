import {
    MOSTRAR_ALERTA,
    OCULTAR_ALERTA
} from '../types';

//Muestar una alerta

export function mostrarAlerta(alerta) {
    return (dispatch) => {
        dispatch(mostrarAlertaError(alerta));
    }
}

const mostrarAlertaError = alerta => ({
    type: MOSTRAR_ALERTA,
    payload: alerta
});

//Ocultar alerta
export function ocultarAlertaAction(alerta) {
    return (dispatch) => {
        dispatch(ocultarAlerta(alerta));
    }
}


const ocultarAlerta = alerta => ({
    type: OCULTAR_ALERTA,
});
