import {
    AGREGAR_PRODUCTO,
    AGREGAR_PRODUCTO_ERROR,
    AGREGAR_PRODUCTO_EXITO,
    COMENZAR_DESCARGA_PRODUCTOS,
    DESCARGA_PRODUCTOS_EXITO,
    DESCARGA_PRODUCTOS_ERROR,
    OBTENER_PRODUCTO_ELIMINAR,
    PRODUCTO_ELIMINADO_EXITO,
    PRODUCTO_ELIMINADO_ERROR,
    OBTENER_PRODUCTO_EDITAR,
    PRODUCTO_EDITADO_EXITO,
    PRODUCTO_EDITADO_ERROR,
    COMENZAR_EDICION_PRODUCTO
} from '../types';

import clienteAxios from '../config/axios'
import Swal from 'sweetalert2';

// Crear nuevos productos
export function crearNuevoProducto(producto) {
    return async (dispatch) => {
        dispatch(agregarProducto());

        try {
            // insertar en el API
            await clienteAxios.post('/productos', producto);

            //Si todo sale bien actualizar el satte
            dispatch(agregarProductoExito(producto));

            //Alerta
            Swal.fire(
                'Correcto',
                'el producto se agregó correctamente',
                'success'
            )
        } catch (error) {
            console.log(error)

            //si hay un error cambiar el state
            dispatch(agregarProductoError(true));

            //Alerta de error
            Swal.fire({
                icon: 'error',
                title: 'Hubo un error',
                text: 'Hubo un error, intente de nuevo'
            })
        }
    }
}

const agregarProducto = () => ({
    type: AGREGAR_PRODUCTO,
    payload: true
})

//Si el producto se guarda en la base de datos
const agregarProductoExito = (producto) => ({
    type: AGREGAR_PRODUCTO_EXITO,
    payload: producto
})

// Si hubo un error
const agregarProductoError = (estado) => ({
    type: AGREGAR_PRODUCTO_ERROR,
    payload: estado
})


//Función que descarga los productos de la base de datos
export function obtenerProductosAction() {
    return async (dispatch) => {
        dispatch(descargarProductos());

        try {
            const respuesta = await clienteAxios.get('/productos');
            dispatch(descargaProductosExitosa(respuesta.data))

        } catch (error) {

            dispatch(descargaProductosError())
        }
    }
}

const descargarProductos = () => ({
    type: COMENZAR_DESCARGA_PRODUCTOS,
    payload: true
})

const descargaProductosExitosa = (productos) => ({
    type: DESCARGA_PRODUCTOS_EXITO,
    payload: productos
})

const descargaProductosError = () => ({
    type: DESCARGA_PRODUCTOS_ERROR,
    payload: true
})

//Selecciona y elimina producto
export function borrarProductoAction(id) {
    return async (dispatch) => {
        dispatch(obtenerProductoEliminar(id))
        try {

            await clienteAxios.delete(`/productos/${id}`);
            dispatch(eliminarProductoExito(id))
            //Si se elimina mostrar alerta
            Swal.fire(
                'Eliminado!',
                'El producto se elimino correctamente.',
                'success'
            )
        } catch (error) {
            dispatch(eliminarProductoError(id))
        }
    }
}

const obtenerProductoEliminar = (id) => ({
    type: OBTENER_PRODUCTO_ELIMINAR,
    payload: id
});

const eliminarProductoExito = () => ({
    type: PRODUCTO_ELIMINADO_EXITO,
});

const eliminarProductoError = () => ({
    type: PRODUCTO_ELIMINADO_ERROR,
});

//Selecciona y EDITA producto
export function editarProductoAction(producto) {
    return async (dispatch) => {
        dispatch(obtenerProductoEditar(producto))
    }
}

const obtenerProductoEditar = (producto) => ({
    type: OBTENER_PRODUCTO_EDITAR,
    payload: producto
});

// editar un registro en el api y el state
export function actualizarProductoAction(producto) {
    return async (dispatch) => {
        dispatch(guardarProductoEditado());

        try {
            // insertar en el API
           await clienteAxios.put(`/productos/${producto.id}`, producto);

           dispatch( editarProductoExito(producto))
        } catch (error) {
            dispatch( editarProductoError(producto))
        }
    }
}

const guardarProductoEditado = () => ({
    type: COMENZAR_EDICION_PRODUCTO,
})

//Si el producto se guarda en la base de datos
const editarProductoExito = (producto) => ({
    type: PRODUCTO_EDITADO_EXITO,
    payload: producto
})

const editarProductoError = () => ({
    type: PRODUCTO_EDITADO_ERROR,
})



