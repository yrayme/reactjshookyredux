import { combineReducers } from 'redux';
import red_Productos from './red_Productos';
import red_alerta from './red_alerta'

export default combineReducers ({
    productos: red_Productos,
    alerta: red_alerta
});